import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { IonPullUpFooterState } from 'ionic-pullup';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-fuel-consumption',
  templateUrl: 'fuel-consumption.html',
})
export class FuelConsumptionPage implements OnInit {
  footerState: IonPullUpFooterState;
  islogin: any;
  portstemp: any[] = [];
  veh_id: any;
  _vehId: any;
  fuelList: any[] = [];
  datetimeStart: any;
  datetimeEnd: any;
  currency_code: any;
  
  constructor(
    public apiCall: ApiServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public event: Events,
    private translate: TranslateService
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();
    if (localStorage.getItem('default_curreny') !== null) {
      this.currency_code = localStorage.getItem('default_curreny');
    }
    this.event.subscribe('reloadFuellist', () => {
      if (this._vehId != undefined) {
        this.getFuelList();
      }
    })
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter FuelConsumptionPage');
    this.getDefaultUserSettings();
  }

  measurementUnit: string = 'MKS';
  getDefaultUserSettings() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
  }
  ngOnInit() {
    this.getdevices();
  }
  toggleFooter() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }

  addFuelEntry() {
    this.navCtrl.push(FuelEntryPage, {
      portstemp: this.portstemp
    })
  }

  getdevices() {
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getFuelList() {
    this.fuelList = [];
    if (this._vehId == undefined) {
      let toast = this.toastCtrl.create({
        message: this.translate.instant('Please select the vehicle first..'),
        position: 'bottom',
        duration: 1500
      })
      toast.present();
    } else {
      var _baseURL = this.apiCall.mainUrl + "fuel/getFuels?vehicle=" + this._vehId + "&user=" + this.islogin._id;
      this.apiCall.startLoading().present();
      this.apiCall.getSOSReportAPI(_baseURL)
        .subscribe(data => {
          this.apiCall.stopLoading();
          console.log("fule entry list: ", data)
          if (!data.message) {
            this.fuelList = data;
          }
        },
          err => {
            this.apiCall.stopLoading();
            let toast = this.toastCtrl.create({
              message: this.translate.instant("Report not found!"),
              duration: 1500,
              position: "bottom"
            })
            toast.present();
          })
    }

  }

  getid(veh) {
    this._vehId = veh._id;
  }

  footerExpanded() {
    console.log('Footer expanded!');
  }

  footerCollapsed() {
    console.log('Footer collapsed!');
  }
}

@Component({
  templateUrl: './fuel-entry.html',
  selector: 'page-fuel-consumption'
})

export class FuelEntryPage {
  portstemp: any[] = [];
  tot_odo: any;
  fuelType: any = 'CNG';
  liter: any = 0; amt: any = 0;
  ddate: any = new Date().toISOString();
  vehName: any;
  veh_id: any;
  _vehId: any;
  islogin: any;

  constructor(
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public event: Events,
    private translate: TranslateService) {
    this.portstemp = this.navParams.get("portstemp");
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
  }

  ionViewDidEnter() {
    this.getDefaultUserSettings();
  }

  measurementUnit: string = 'MKS';
  getDefaultUserSettings() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
  }

  onChnageEvent(veh) {
    console.log("vehicle info:", veh)
    debugger
    let that = this;
    that.veh_id = veh._id;
    var sb = veh.total_odo;
    that.tot_odo = sb.toFixed(2);
  }

  fixDecimals(value: string) {
    value = "" + value;
    value = value.trim();
    value = parseFloat(value).toFixed(2);
    return value;
  }

  submit() {
    let that = this;
    debugger
    if (this.liter == undefined || this.amt == undefined || this.tot_odo == undefined || this.fuelType == undefined || this.ddate == undefined || this.vehName == undefined) {
      let toast = this.toastCtrl.create({
        message: this.translate.instant("Please fill all the mandatory fields.."),
        position: 'bottom',
        duration: 2000
      })
      toast.present();
    } else {
      var payload = {};
      var _baseURL = this.apiCall.mainUrl + "fuel/addFuel";
      payload = {
        "vehicle": that.veh_id,
        "user": that.islogin._id,
        "date": new Date(that.ddate).toISOString(),
        "quantity": that.liter,
        "odometer": that.tot_odo,
        "price": that.amt,
        "fuel_type": that.fuelType,
        "comment": "First fill"
      }

      this.apiCall.startLoading().present();
      this.apiCall.urlpasseswithdata(_baseURL, payload)
        .subscribe(data => {
          this.apiCall.stopLoading();
          console.log("fuel entry added: " + data);
          this.liter = null;
          this.amt = null;
          this.tot_odo = null;
          this.fuelType = null;
          this.ddate = null;
          this.vehName = null;
          this.veh_id = null;

          let toast = this.toastCtrl.create({
            message: this.translate.instant("Fuel entry added successfully! Add another entry.."),
            position: 'bottom',
            duration: 2000
          })
          toast.present();
          this.event.publish('reloadFuellist');

        })

    }
  }
}
