import { ViewController, ToastController, NavParams } from "ionic-angular";
import { Component } from "@angular/core";
import { ApiServiceProvider } from "../../../../providers/api-service/api-service";
@Component({
    templateUrl: './report-setting.html',
    styles: [`
    .mainDiv {
        padding: 50px;
        
    }

    .secondDiv {
        padding-top: 20px;
        border-radius: 18px;
        border: 2px solid black;
        background: white;
    }`]
})


export class ReportSettingModal {
    reportArray = [
        { name: "Daily Report", Rstatus: true, Astatus: false, value: "daily_report" },
        { name: "Daywise Report", Rstatus: true, Astatus: false, value: "daywise_report" },
        { name: "Speed Variation", Rstatus: true, Astatus: false, value: "speed_variation" },
        { name: "Fuel Report", Rstatus: true, Astatus: false, value: "fuel_report" },
        { name: "Idle Report", Rstatus: true, Astatus: false, value: "idle_report" },
        { name: "Fuel Consumption Report", Rstatus: true, Astatus: false, value: "fuel_consumption_report" },
        { name: "Trip Report", Rstatus: true, Astatus: false, value: "trip_report" },
        { name: "Travel Path Report", Rstatus: true, Astatus: false, value: "travel_path_report" },
        { name: "Summary Report", Rstatus: true, Astatus: false, value: "summary_report" },
        { name: "Geofence Report", Rstatus: true, Astatus: false, value: "geofence_report" },
        { name: "Overspeed Report", Rstatus: true, Astatus: false, value: "overspeed_report" },
        { name: "Route Violation Report", Rstatus: true, Astatus: false, value: "route_violation_report" },
        { name: "Stoppage Report", Rstatus: true, Astatus: false, value: "stoppage_report" },
        { name: "Ignition Report", Rstatus: true, Astatus: false, value: "ignition_report" },
        { name: "Distance Report", Rstatus: true, Astatus: false, value: "distance_report" },
        { name: "POI Report", Rstatus: true, Astatus: false, value: "poi_report" },
        { name: "SOS Report", Rstatus: true, Astatus: false, value: "sos_report" },
        { name: "AC Report", Rstatus: true, Astatus: false, value: "ac_report" },
        { name: "Driver Performance Report", Rstatus: true, Astatus: false, value: "driver_performance_report" },
        { name: "User Trip Report", Rstatus: true, Astatus: false, value: "user_trip_report" },
        { name: "Alert Report", Rstatus: true, Astatus: false, value: "alert_report" }
    ];
    islogin: any;
    params: any;
    constructor(
        private viewCtrl: ViewController,
        private apiCall: ApiServiceProvider,
        private toastCtrl: ToastController,
        private navParams: NavParams
    ) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.params = navParams.get('param');
        console.log("navparams: ", this.params);
        this.getCostumerDetail();
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    submit() {
        var report_preference_payload = [];

        report_preference_payload = JSON.parse(JSON.stringify(this.reportArray));
        var tempsemp = {};

        console.log(report_preference_payload);
        for (var index in report_preference_payload) {
            var k = report_preference_payload[index].value;

            if (report_preference_payload[index].Rstatus == false) {
                report_preference_payload[index].Astatus = false;
            }
            tempsemp[k] = report_preference_payload[index];
            delete tempsemp[k].name;
            delete tempsemp[k].value;
        }
        console.log(tempsemp);
        var reportPref = {
            reportsArr: tempsemp,
            id: this.params._id
        }
        var url = this.apiCall.mainUrl + "users/reportPrefrence";
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(url, reportPref).subscribe(res => {
            console.log("check status: ", res);
            this.apiCall.stopLoading();
            this.toastCtrl.create({
                message: 'Report preference updated',
                duration: 1000,
                position: 'top'
            }).present();
        }, err => {
            this.apiCall.stopLoading();
            console.log("error", err);
        })
    }

    selectedStat(ind, ev, flag) {
        if (flag == 'rVisibility') {
            this.reportArray[ind].Rstatus = ev.checked;
        }
        // console.log('this.reportArray', this.reportArray);
    }

    getCostumerDetail() {
        let that = this;
        this.apiCall.getcustToken(this.params._id).subscribe(res => {
            // console.log("resresresresresresresresres", res.cust.report_preference);
            var tempReport = res.cust.report_preference;
            for (let dt in tempReport) {

                let arr = that.reportArray.filter(function (d) {
                    return d.value == dt;
                })
                arr[0].Astatus = tempReport[dt].Astatus;
                arr[0].Rstatus = tempReport[dt].Rstatus;

            }
            // console.log("that.reportArray=>", that.reportArray);
        })
    }
}